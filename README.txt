Note
====

A teminal quick notes app, written in Rust.

At the prompt, type your notes. when you start a note with ":", it is a command.
valid commands are:
  q      |  quit
  h      |  help
  r n    |  read from line n

Compilation:

Install rust.
        Arch: # pacman -S rust
        Debian: # apt install cargo
        Other: $ curl https://sh.rust.rs -sSf | sh
Clone this repo.
		$ git clone https://bitbucket.org/BigCaballito/rust-note.git
Build the thing.
		$ cd rust-note
        $ cargo build --release
        $ ./target/release/note

Enjoy :)
