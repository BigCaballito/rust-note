use std::io;
use std::io::Write;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::io::BufReader;

static NOTEPATH: &str = "/tmp/notefile";

fn main() {
    let mut notefile = OpenOptions::new()
        .append(true)
        .create(true)
        .open(NOTEPATH)
        .unwrap();
    
    let mut nte: (u32, String) = (0,String::new());
    let mut it: u64 = 0;

    loop {
        it += 1;
        nte = note(it);
        match nte.0 {
            1 => match handle_cmd(&nte.1) {
                Ok(1) => break,
                Ok(_) => (),
                Err(_) => println!("Invalid command. Enter :h for a list of valid commands."),
            }
            _ => write!(notefile, "{}", nte.1).expect("Failed to write to line"),
        }
    }
    
}

fn note(it: u64) -> (u32, String) {
    print!("{} >>> ", it);
    io::stdout().flush()
        .expect("Failed to flush stdout");
    let mut nte = String::new();
    io::stdin().read_line(&mut nte)
        .expect("Failed to read line.");
    match &nte[0..1] {
        ":" => (1, nte),
        _ => (0, nte),
    }
}

fn handle_cmd(cmd: &String) -> Result<u32, u32> {
    match &cmd[0..2] {
        ":q" => return Ok(1),
        ":r" => {
            let arg: usize = match cmd[3..].trim().parse() {
                Ok(num) => num,
                Err(why) => {
                    println!("Couldn't parse read argument: {}", why);
                    1
                },
            };
            let mut nf = OpenOptions::new()
                .read(true)
                .open(NOTEPATH)
                .unwrap();
            let reader = BufReader::new(nf);
            let lines: Vec<String> = reader.lines()
                .map(|l| l.expect("Failed to vectorize file"))
                .collect();
            println!("{:?}", &lines[arg..]);
        }
        ":h" => println!(":q = quit\n:r [n] = read [n lines]"),
        ":help" => println!(":q = quit\n:r [n] = read [n lines]"),
        _ => return Err(1),
    }
    Ok(0)
}
